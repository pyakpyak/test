# sorter #

## How to start ###

sort.py <fileName>.

Should work with python2.7, no external dependencies.

The program does some minimal verification if file exists and each record in the file has at least 2 fields in order to be counted. 

We don't verify correctnes of the fields itself (e.g. timestamp is parseable to int) and assume they're always correct.

## Algorithm ##

* Iterate through all records and group records into daily buckets. Inside each bucket we count each unique entry, but data is not sorted. O(N)
* Sort and iterate through all daily buckets. Binary tree structure is used for sorting (short description below). avg O(logn) to insert if data is randomized, O(n) to traverse 
* Sort records in each bucket and iterate. The same binary tree is used for sorting, but in reversed order, the same complexity, but based on total number of unique entries in each daily bucket

## Some highlights ##

* Sorter - simple binary tree for data sorting, has to be parametized with a "key extractor" function (depends on the data structure we want to sort) and optional `reversed` flag. I chose it for implementation simplicity for this excercise, although i understand that in worse case scenario it may be unbalanced (we may need self balancing implementation). It also might be some other sorting algo, there are many way to solve it. 

* PeriodBucket - a structure to group and count entries within a particular time frame (24 hours by default)
* RecordBucket - a structure to count occurences for each unique entries

* I have not done any real timezone conversion for the results and print the results in UTC (0 offset, no tz)