
import time, sys, os
from datetime import datetime


class SorterNode:
    """Sorting tree node"""

    def __init__(self, key, reversed=False):
        self.key = key
        self.values = [] # all values with the same key aggregated under the same node

        self.reversed=reversed

        self.lt = None
        self.gt = None

    def __repr__(self):
        return "<sorter node {} l:{} r:{}>".format(self.key, self.lt, self.gt)

    def addEntry(self, key, value):
        """adds an entry to the node"""

        if self.__cmp(key, self.key, self.reversed) == 0:
            self.values.append(value)

        if self.__cmp(key, self.key, self.reversed) > 0:
            if self.gt is None:
                self.gt = SorterNode(key, self.reversed)

            self.gt.addEntry(key, value)

        if self.__cmp(key, self.key, self.reversed) < 0:
            if self.lt is None:
                self.lt = SorterNode(key, self.reversed)

            self.lt.addEntry(key, value)

    def __cmp_(self, k1, k2):
        """basic cmp function"""
        if k1 > k2:
            return 1
        if k1 < k2:
            return -1
        return 0

    def __cmp(self, k1, k2, reversed=False):
        """cmp function wrapper to support reversed order"""
        if reversed:
            return self.__cmp_(k1, k2) * -1
        return self.__cmp_(k1, k2)


class Sorter:
    """Sorting tree"""

    def __init__(self, keyFunc, reversed=False):
        self.root = None
        self.keyFunc = keyFunc

        self.reversed = reversed

    def __repr__(self):
        return "{}".format(self.root)

    def add(self, entry):
        key = self.keyFunc(entry)

        if self.root is None:
            self.root = SorterNode(key, self.reversed)

        self.root.addEntry(key, entry)

    def addAll(self, entries):
        for e in entries:
            self.add(e)

    def iterateInOrder(self):
        """in order tree traversal. It uses stack to eliminate recursion.
        """

        stack = []
        stack.append(self.root)

        currentElement = self.root

        # initializing initial stack
        while currentElement.lt:
            stack.append(currentElement.lt)
            currentElement = currentElement.lt

        # start
        while stack:
            currentElement = stack.pop(-1)

            for v in currentElement.values:
                yield v

            if currentElement.gt:
                currentElement = currentElement.gt
                stack.append(currentElement)

                while currentElement.lt:
                    stack.append(currentElement.lt)
                    currentElement = currentElement.lt

class RecordBucket:
    """ bucket which represents unique record entry with number of its occurences """

    def __init__(self, record):
        self.record = record
        self.count = 0

    def __repr__(self):
        return "<record {} {}>".format(self.record, self.count)

    def __str__(self):
        return "{} {}".format(self.record, self.count)

    def increment(self):
        self.count = self.count + 1


class PeriodBucket(object):
    """ bucket which represents a time interval with corresponding entries"""

    def __init__(self, ts):
        self.ts = ts
        self.records = {}

    def __repr__(self):
        return "<period bucket {}>".format(self.ts)

    def __str__(self):
        return datetime.utcfromtimestamp(self.ts).strftime("%m/%d/%Y")

    def addRecord(self, rec):
        bucket = self.records.get(rec)
        if bucket is None:
            bucket = RecordBucket(rec)
            self.records[rec] = bucket
        bucket.increment()

    def getRecords(self):
        return self.records.values()


def readFile(fileName):
    with open(fileName) as f:
        for l in f:
            fields = l.strip().split("|")
            if len(fields) >= 2:
                ts = int(fields[0])
                url = fields[1]

                yield ts, url


def getBucketTs(ts, bucketSize=86400):
    """ calculates period start time (aka bucket id) for a given ts"""
    return ts-ts%bucketSize

def run(fileName):
    periods = {}

    for recordTs, url in readFile(fileName):
        bucketTs = getBucketTs(recordTs)

        #print(bucketTs, recordTs, url)

        currentBucket = periods.get(bucketTs)
        if currentBucket is None:
            currentBucket = PeriodBucket(bucketTs)
            periods[bucketTs] = currentBucket

        currentBucket.addRecord(url)

    # sorter for period buckets (days)
    periodsSorter = Sorter(keyFunc=lambda x: x.ts)
    periodsSorter.addAll(periods.values())

    for periodBucket in periodsSorter.iterateInOrder():
        print periodBucket

        # sorter for records inside period bucket, reversed order
        recordsSorter = Sorter(keyFunc=lambda x: x.count, reversed=True)
        recordsSorter.addAll(periodBucket.getRecords())

        for recordBucket in recordsSorter.iterateInOrder():
            print recordBucket


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "command format: sort.py <fileName>"
        sys.exit(1)

    fileName = sys.argv[1]
    if not os.path.isfile(fileName):
        sys.stderr.write("file doesn't exist {}\n".format(fileName))
        sys.exit(1)

    run(fileName)